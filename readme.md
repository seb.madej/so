```
USAGE
          On terminal

              so [-tag] [commands]

          On scripts

              so () {  /bin/so "-${FUNCNAME[1]}" "${@}"  }

              so [commands]

LEGEND
          [commands]
              The commands to execute.

          [-tag]
              Optionally  a tag to show before any error, to help pinpoint its
              location. [commands] is automatically appended at the end of the
              tag. If you only type '-', only [commands] forms the tag.

NOTES
       -  Danger: if [commands] asks any question, 'so' won't answer it by it‐
       self. Instead it will wait forever for the command to terminate.

       - Only the last 100 lines of errors will be printed.
